rpm
=====


Quick cheat sheet you will find handy while using rpm at shell prompt:

* Installing rpm package::

	rpm -ivh <rpm_file>
	
  **Usage**: `rpm -ivh httpd-2.0.49-4.i386.rpm`


* Upgrage rpm package::
        
	rpm -Uvh <rpm_file>

  **Usage**: `rpm -Uvh httpd-2.0.49-4.i386.rpm`

* Remove rpm package::
        
	rpm -ev <rpm_file>

  **Usage**: `rpm -ev httpd-2.0.49-4.i386.rpm`
  
* Remove rpm package without removing dependencies::
        
	rpm -ev --nodeps <rpm_file>

  **Usage**: `rpm -ev --nodeps httpd-2.0.49-4.i386.rpm`
  
* Query all rpm package ::
        
	rpm -qa

  **Usage**: `rpm -qa`

* Query rpm package for short description::
        
	rpm -qi

  **Usage**: `rpm -qi perl`  

* Query rpm package for short description::
        
	rpm -qi

  **Usage**: `rpm -qi perl`  

* Find out what rpm package a file belongs::
        
	rpm -qf <path_to_file>

  **Usage**: `rpm -qf /etc/passwd`  
  
* Find out what rpm package a file belongs::
        
	rpm -qf <path_to_file>

  **Usage**: `rpm -qf /etc/passwd`  

* Find out package configuration file::
        
	rpm -qc <package_name>

  **Usage**: `rpm -qc httpd`  
  
* Display list of configuration files for a command::
        
	rpm -qcf <path_to_file>

  **Usage**: `rpm -qcf /usr/X11R6/bin/xeyes`  

* Display list of all recently installed RPMs::
        
	rpm -qa --last 	

  **Usage**: `rpm -qa --last`   

* Find out what dependencies a rpm file has::
        
	rpm -qR <package_name>

  **Usage**: `rpm -qR mediawiki-1.4rc1-4.i586.rpm`   
