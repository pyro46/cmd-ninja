sed 
===

* Match whole string::
	
	sed "s/\b<keyword\b/g"

  **Usage**: echo "bar foobar" | sed "s/\\bbar\\b/no\ bar/g"
