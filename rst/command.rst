Command 
=======

At Shell Prompt
---------------

* Running previous command with sudo::
	
	ls -la 
	sudo !!

  **Usage**: This will run `ls -la` command using sudo command 

* Using !! on shell prompt:: 

  	ls -la a 
	echo !!:2 
  
  **Usage**: This will print `a` on screen.

* Using !! to replace some part of command:: 

	echo "foo"
	!!:gs/foo/bar 

  **Usage**: Runs previous command replacing foo by bar every time that foo appears 

* whoami in mysql database:: 
	
	mysql> select user(),current_user();

Perl
---- 

* Edit file contents:: 

	perl -pi.bak -e "tr/[a-z]/[A-Z]/" sample.txt
  
  **Usage**: Creates backup of original file and replace text in given file.

* Replace text in file and creating backup of file::

        perl -pi.bak -e '<replacement>' <filename>

  **Usage**: `perl -pi.bak -e 'tr/[a-z]/[A-Z]/g' sample.txt`
  
  This will convert text of sample.txt to uppercase and create backup of sample.txt as sample.txt.bak 


Process
-------

* Find infomartion about process by name:: 

        ps ax | grep <process_name> 
        ps ax | grep <process_name> | grep -v grep 
        
  **Usage**:

  * `ps ax | grep apache2`
  * `ps ax | grep [Aa]pache2` #This will only show apache process (without grep)
  * `ps ax | grep apache2 | grep -v grep`


Networking
----------

* Shows the network connections::
        
        ifconfig

* Show ip address for all available interfaces::
  
        ip addr | grep inet 

* Start webserver serving current directory tree at http://localhost:8000/::
        
        python -m SimpleHTTPServer 

* Find who owns port number::

        fuser -v -n tcp <port_number>
  
  **Usage**: `fuser -v -n tcp 6000` 


Kernel and Kernal module
------------------------

* Shows the status of modules in the Linux Kernel::

        lsmod 
        
* Shows the list of modules for Linux Kernel::
        
        ls /lib/modules/$(uname -r) -R  | grep ko
* Finding hardware information::
        
        lshw 

 **Usage**: `lshw -c video` #This will provide information about `video` hardware.

.. Note::
        You need to be super-user to run this command and get all information about hardware.

* Show information about particular module::

        modinfo <driver_name> 

  **Usage** : `modinfo nvidia` #This will provide information about nvidia driver 


Swap
----

* More swap with a swap file:: 
          
        dd if=/dev/zero of=/swapfile bs=1024 count=65536 #Create 64MB swap file on your root partition
        mkswap /swapfile 65536          #convert file to swap file  
        sync 
        swapon /swapfile        #add swapfile to your swapspace 

Memory
------ 

* Check memory status of::
	
	head -2 /proc/meminfo 

  **Usage**: provide Available and Used memory of current state of machine 

* View the different caches and their sizes::

	sudo head -2 /proc/slabinfo; sudo cat /proc/slabinfo | egrep dentry\|inode 

  **Courtesy**: ``http://linuxaria.com/howto/linux-memory-management``

wget
----

* Download list of files using `wget`:: 

        cat <file_containing_url> | xargs wget -c 

  **Usage** : `cat urlist.txt | xargs wget -c` #download content of url for urlist.txt 

