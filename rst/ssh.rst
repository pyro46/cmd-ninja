ssh
===

* Per-host SSH client configuration options

  You can set per-host configuration options in ~/.ssh/config by specifying Host hostname, followed by host-specific options.
  It is possible to set the private key and the user (among many other settings) on a per-host basis.

  Here's an example config file::

		Host abhijeet
		User admin
		IdentityFile ~/.ssh/admin.id_dsa

  or::
 
		Host somehost
		User dev
		HostName example.com
		IdentityFile ~/.ssh/dev.id_dsa

* Add public key to remote server::

	ssh-copy-id -i ~/.ssh/id_rsa.pub username@hostname 
	
  **Usage**: Copy public key to remote server 