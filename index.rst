.. Command Ninja documentation master file, created by
   sphinx-quickstart on Sun Jun  3 07:27:33 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Command Ninja
=============

Command Ninja is collection of frequently used Linux and other operating systems' commands
which help you to do work more efficiently and swiftly.


Contents:

.. toctree::
   :maxdepth: 1 

   rst/command
   rst/alias
   rst/bashrc
   rst/grep
   rst/find
   rst/sed
   rst/regex
   rst/git
   rst/dpkg
   rst/apt
   rst/rpm
   rst/ssh


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

